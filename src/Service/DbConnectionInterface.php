<?php

namespace AndriiKorpusov\DbConnector\Service;

interface DbConnectionInterface
{
    public function insert($object);

    public function update($object);

    public function delete($object);

    public function select($object, $params);
}