<?php

namespace AndriiKorpusov\DbConnector\Service\Helpers;

class DataMapper
{
    public function arrayToData($object, $data) {
        $properties = ReflectionApi::getPrivateProperties($object);

        foreach ($properties as $property) {
            $column = $property->getName();
            $setter = sprintf('set%s', ucfirst($column));
            if (method_exists($object, $setter) && isset($data[$column])) {
                $object->$setter($data[$column]);
            }
        }

        return $object;
    }
}